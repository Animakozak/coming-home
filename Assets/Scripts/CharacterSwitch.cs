﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSwitch : MonoBehaviour
{
	public GameObject Current;
	[SerializeField] private GameObject Mom;
	[SerializeField] private GameObject Dad;
	[SerializeField] private GameObject Child;
	
	[SerializeField] private RawImage MomAvatar;
	[SerializeField] private RawImage DadAvatar;
	[SerializeField] private RawImage ChildAvatar;
	
	[SerializeField] private Texture MomImg;
	[SerializeField] private Texture DadImg;
	[SerializeField] private Texture ChildImg;
	[SerializeField] private Texture _MomImg;
	[SerializeField] private Texture _DadImg;
	[SerializeField] private Texture _ChildImg;

	[SerializeField] private CinemachineVirtualCamera CamMom;
	[SerializeField] private CinemachineVirtualCamera CamDad;
	[SerializeField] private CinemachineVirtualCamera CamChild;

	
	void Start ()
	{
		PickMom();
	}
	
	
	void Update ()
	{
		if (Current == Mom)
		{
			CamMom.Priority = 11;
			CamChild.Priority = 10; 
			CamDad.Priority = 10;
		}
		else if (Current == Dad)
		{
			CamMom.Priority = 10;
			CamChild.Priority = 10; 
			CamDad.Priority = 11;
		}
		else
		{
			CamMom.Priority = 10;
			CamChild.Priority = 11; 
			CamDad.Priority = 10;
		}
	}

	public void PickMom()
	{
		Current = Mom;
		Current.GetComponent<PlayerMovement>().enabled = true;
		Dad.GetComponent<PlayerMovement>().enabled = false;
		Child.GetComponent<PlayerMovement>().enabled = false;

		MomAvatar.texture = MomImg;
		DadAvatar.texture = _DadImg;
		ChildAvatar.texture = _ChildImg;
	}

	public void PickDad()
	{
		Current = Dad;
		Current.GetComponent<PlayerMovement>().enabled = true;
		Mom.GetComponent<PlayerMovement>().enabled = false;
		Child.GetComponent<PlayerMovement>().enabled = false;
		
		MomAvatar.texture = _MomImg;
		DadAvatar.texture = DadImg;
		ChildAvatar.texture = _ChildImg;
	}

	public void PickChild()
	{
		Current = Child;
		Current.GetComponent<PlayerMovement>().enabled = true;
		Mom.GetComponent<PlayerMovement>().enabled = false;
		Dad.GetComponent<PlayerMovement>().enabled = false;
		MomAvatar.texture = _MomImg;
		DadAvatar.texture = _DadImg;
		ChildAvatar.texture = ChildImg;
	}
}
