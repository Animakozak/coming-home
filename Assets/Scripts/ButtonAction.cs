﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ButtonAction : MonoBehaviour
{
    public Sprite Alternative;
    public bool VerticalHoriznotal;
    public bool UnlockLadder;
    public Animator anim1;
    public Animator anim2;
    public Animator anim3;
    public GameObject ladder;
    public GameObject secret_ladder;
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Triggered!");
        if (!UnlockLadder)
        {
            if (other.gameObject.CompareTag("Player"))
            {

                //move horizontal animation
                anim1.SetBool("OpenVert", true);
                anim2.SetBool("OpenVert", true);
                if(VerticalHoriznotal) GetComponent<SpriteRenderer>().sprite = Alternative;
            }
            else if (other.gameObject.CompareTag("Pickable"))
            {
                anim3.SetBool("OpenHor", true);
                GetComponent<SpriteRenderer>().sprite = Alternative;
            }

            
        }
        if (other.gameObject.CompareTag("Player") && UnlockLadder)
        {

            ladder.SetActive(false);
            secret_ladder.GetComponent<BoxCollider2D>().isTrigger = true;

            
        }
    }
}
