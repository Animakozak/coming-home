﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public CharacterController2D controller;
	private bool isJumping;
	public bool isTunnel;
	private float movement;
	public float speed = 10;

	public GameObject item;
	private bool isPicked;
	public bool canPick;
	public bool isLadder;

	private void Start()
	{
		item = null;
	}

	void Update()
	{
		movement = Input.GetAxisRaw("Horizontal") * speed;
		if (Input.GetButtonDown("Jump"))
		{
			if (!isPicked) isJumping = true;
		}

		if (canPick)
		{
			if (item != null)
			{
				if (Input.GetButtonDown("Fire1"))
				{
					Debug.Log("Button pressed");
					if (!isPicked)
					{
						item.gameObject.transform.SetParent(this.transform);
						item.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
						isPicked = true;
					}
					else
					{
						item.gameObject.transform.SetParent(null);
						item.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
						isPicked = false;
//						item = null;
					}
				}
			}
		}
	}

	void FixedUpdate()
	{
		controller.Move(movement*Time.fixedDeltaTime, false, isJumping);
		if (isLadder)
		{
			GetComponent<Rigidbody2D>().isKinematic = true;
		}
		else
		{
			GetComponent<Rigidbody2D>().isKinematic = false;
			isJumping = false;
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Pickable"))
		{
			if (item == null)
			{
				item = other.gameObject;
			}
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Pickable"))
		{
			Debug.Log("Exit");
			if (item == other.gameObject && !isPicked)
			{
				item = null;
			}
		}
	}

//	private void OnTriggerEnter2D(Collider2D other)
//	{
//		if (other.gameObject.CompareTag("Button"))
//		{
//			other.GetComponent<Interactions>().Activate();
//		}
//	}
}