﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour
{
    public CharacterController2D Controller;
    public bool CanPickObjects;
    public float Speed = 10;

    private bool _isJumping;
    private float _movement;

    private bool _isPicked = false;
    private GameObject _pickableObject;

    void Update()
    {
        _movement = Input.GetAxisRaw("Horizontal") * Speed;
        _isJumping = Input.GetAxisRaw("Vertical") > 0
            ? (!_isPicked ? true : false)
            : false;

        if (_pickableObject != null)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (!_isPicked)
                {
                    _pickableObject.transform.parent = Controller.transform;
                    _isPicked = true;
                }
                else if (_isPicked)
                {
                    _pickableObject.transform.parent = null;
                    _isPicked = false;
                }
            }
        }
    }

    void FixedUpdate()
    {
        Controller.Move(_movement * Time.fixedDeltaTime, false, _isJumping);
        _isJumping = false;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Pickable"))
        {
            if (_pickableObject == null)
            {
                _pickableObject = collision.gameObject;
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Pickable"))
        {
            if ((collision.gameObject == _pickableObject) && !_isPicked)
            {
                _pickableObject = null;
            }
        }
    }
}
