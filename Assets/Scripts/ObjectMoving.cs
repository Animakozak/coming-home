﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMoving : MonoBehaviour
{
    public CharacterController2D _controller;
    private bool _isPicked;
    private GameObject _pickableObject;

    void Update()
    {
        if (_pickableObject != null)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (!_isPicked)
                {
                    _pickableObject.transform.parent = _controller.transform;
                    _isPicked = true;
                }
                else if (_isPicked)
                {
                    _pickableObject.transform.parent = null;
                    _isPicked = false;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    { 
        if (collision.gameObject.tag.Equals("Pickable"))
        {
            if (_pickableObject == null)
            {
                _pickableObject = collision.gameObject;
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Pickable"))
        {
            if ((collision.gameObject == _pickableObject) && !_isPicked)
            {
                _pickableObject = null;
            }
        }
    }
}
