﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnFirst : MonoBehaviour
{
    public bool IsActive = false;
    public Sprite btnFirstSprite;
    public Sprite btnThirdSprite;
    public GameObject btnThird;

    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (btnFirstSprite != null)
                _spriteRenderer.sprite = btnFirstSprite;

            if (btnThirdSprite != null)
                btnThird.gameObject.GetComponent<SpriteRenderer>().sprite = btnThirdSprite;

            btnThird.gameObject.GetComponent<btnThird>().IsActive = true;
        }        
    }
}
