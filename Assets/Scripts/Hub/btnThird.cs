﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnThird : MonoBehaviour
{
    public Animator Platform;
    public Sprite btnSecondSprite;
    public bool IsActive = false;

    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (IsActive)
        {
            if (col.gameObject.CompareTag("Pickable"))
            {
                Debug.Log("btnThird");
                _spriteRenderer.sprite = btnSecondSprite;

                Platform.SetBool("Activate", true);
                //Активировать анимацию Хаба

            }
        }
    }
}
