﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    public int _playerAmount = 0;


    void Update()
    {
        if (_playerAmount > 5)
        {
            Debug.Log("Next Level");
            //Событие перехода некст левел
            SceneManager.LoadScene("Details");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerAmount++;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerAmount--;
        }
    }
}
