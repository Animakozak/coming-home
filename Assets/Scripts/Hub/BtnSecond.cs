﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSecond : MonoBehaviour
{
    public Sprite btnSecondSprite;
    public GameObject Box;
    public Sprite boxSprite;

    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (btnSecondSprite != null)
                _spriteRenderer.sprite = btnSecondSprite;

            if (boxSprite != null)
            {
                Box.gameObject.GetComponent<SpriteRenderer>().sprite = boxSprite;
                Box.gameObject.AddComponent<Rigidbody2D>();
                Box.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
                //Box.gameObject.GetComponent<Rigidbody2D>().mass = 3;
                Box.gameObject.AddComponent<BoxCollider2D>();
                Box.gameObject.tag = "Pickable";
            }   
        }
    }
}
