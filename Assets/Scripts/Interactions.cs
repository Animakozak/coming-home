﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactions : MonoBehaviour
{
    public Sprite altSprite;
    public Animator animator;

    void Update()
    {
        
    }

    public void Activate()
    {
        GetComponent<SpriteRenderer>().sprite = altSprite;
        animator.SetBool("Activated",true);
    }
    
    
}
